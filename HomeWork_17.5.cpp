#include <iostream>
#include <cmath>

using namespace std;

class Example
{
private:
	int a = 5;
	int b = 10;
public:
	int ShowA()
	{
		return a;
	}
	int ShowB()
	{
		return b;
	}
	int Vector()
	{
		int x = 5;
		int y = 7;
		int z = 3;
		double vm;
		vm = sqrt((x * x) + (y * y) + (z * z));
		return vm;
	}
};

int main()
{
	Example w;
	cout << w.ShowA() << ' ' << w.ShowB() << "\n" << w.Vector();
}